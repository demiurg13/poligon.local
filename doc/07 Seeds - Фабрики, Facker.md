##07 Seeds - Фабрики, Facker

### Создаем фабрику моделей
```
php artisan make:factory BlogPostFactory
```
>Документация: <https://github.com/fzaninotto/Faker>

Запуск сидов
```
php artisan migrate:refresh --seed
```

