##06 Seeds - заполнение БД тестовыми данными

### Создаем сиды
```
php artisan make:seeder UsersTableSeeder
```
```
php artisan make:seeder BlogCategoriesTableSeeder
```
Запуск сидов
php artisan db:seed
php artisan migrate:refresh --seed
